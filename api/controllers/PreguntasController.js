/**
 * PreguntasController
 *
 * @description :: Server-side logic for managing preguntas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	// Lista las preguntas. Elige dos al azar
	listar: function(req, res, next){
		Preguntas
			.find()
			.then(registros => {
				// Genera una propiedad "orden" con un valor aleatorio
				registros.forEach(item => {
					item.orden = Math.random()*10000 + 10
				})

				// Ordena basada en la propiedad orden
				registros.sort( (a,b)=> {
					return a.orden > b.orden
				})

				// Genera el arreglo con las dos preguntas
				let lista = []
				lista.push(registros[0])
				lista.push(registros[1])

				// Envía el arreglo anterior
				res.json(lista)
			})
	}
	
};

