/**
 * RegistrosController
 *
 * @description :: Server-side logic for managing registros
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	// Inserta un registro basado en los datos enviados desde el formulario
	insertar: function(req, res, next){
		const datos = req.allParams()

		console.log(datos)

		Registros
			.create(datos)
			.then(registro => {
				// Devuelve el registro creado
				res.json(registro)
			})
	}
};

