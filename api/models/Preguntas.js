/**
 * Preguntas.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "preguntas",
  attributes: {
  	id: {
  		primaryKey: true,
  		unique: true,
  		autoIncrement: true,
  		type: "integer"
  	},

  	pregunta: "string",
  	alternativa1: "string",
  	alternativa2: "string",
  	alternativa3: "string",
  	correcta: "integer"
  }
};

