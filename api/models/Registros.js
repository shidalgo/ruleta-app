/**
 * Registros.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "registros",
  attributes: {
  	id: {
  		primaryKey: true,
  		unique: true,
  		autoIncrement: true,
  		type: "integer"
  	},

  	nombre: "string",
  	empresa: "string",
  	correo: "string"
  }
};

