const domCargado = () =>{
	// Variables
	const btnIntro = document.querySelector("div.intro button")
	const pnlIntro = document.querySelector("div.intro")
	const pnlPreguntas = document.querySelector("div.preguntas")
	const pnlResultado = document.querySelector("div.resultado")
	const pnlRuleta = document.querySelector("div.ruleta")
	const alternativas = document.querySelectorAll("div.alternativa")
	const btnSeguir = document.querySelector("button.seguir")
	const btnSalir = document.querySelector("button.salir")
	const btnGirar = document.querySelector("div.girar")
	const btnHome = document.querySelector("div.ruleta button")
	const disco = document.querySelector("div.disco")
	const snd = document.querySelector("#sonido")
	const teclado = document.querySelector("div.contenedorTecladoAlfaNumerico")
	const teclas = document.querySelectorAll("div.contenedorTecladoAlfaNumerico ul li a")

	const nombre = document.querySelector("article.nombre")
	const empresa = document.querySelector("article.empresa")
	const correo = document.querySelector("article.correo")

	const textos = document.querySelectorAll("article.texto")

	const enunciado = document.querySelector("div.enunciado")
	const alt01 = document.querySelector("div.alt01")
	const alt02 = document.querySelector("div.alt02")
	const alt03 = document.querySelector("div.alt03")

	let angulo = 0
	let velocidad = 0
	let aceleracion
	let maxVelocidad = 10
	let desacelerar
	let intGiro
	let anguloAnterior = 0
	let puedeResponder = true
	let preguntaActual = -1

	let preguntas
	let numPreguntasAcertadas = 0

	let destinoTexto
	let cerrado = true	

	// Funciones

	// reinicia las variables clave
	const resetear = ()=> {
		numPreguntasAcertadas = 0
		preguntaActual = -1
		puedeResponder = true
		velocidad = 0
		nombre.innerHTML = "Nombre"
		correo.innerHTML = "Correo"
		empresa.innerHTML = "Empresa"
	}

	// registra los datos del usuario
	const ftnRegistro = e => {
		e.preventDefault()

		const nombreP = nombre.innerHTML
		const correoP = correo.innerHTML
		const empresaP = empresa.innerHTML

		if(nombreP!="" && nombreP!="Nombre" && correoP!="" && correoP!="Correo" && empresaP!="" && empresaP!="Empresa") {
			let datos = new FormData()
			datos.append("nombre", nombreP)
			datos.append("empresa", empresaP)
			datos.append("correo", correoP)

			fetch("/insertar", {method: "post", body: datos})
				.then(resultados => {
					return resultados.json()
				})
				.then(registros => {
					mostrarPregunta()

					pnlIntro.style.display = "none"
					pnlPreguntas.style.display = "block"					
				})
		}
	}

	// valida la respuesta del usuario
	const ftnAlternativaSeleccionada = function(e){
		if(!puedeResponder) return false

		const alternativa = this.getAttribute("data-opcion")

		if(alternativa == preguntas[preguntaActual].correcta) {
			numPreguntasAcertadas++
			this.classList.add("acierto")	
		} else {
			this.classList.add("error")
		}

		
		puedeResponder = false

		setTimeout(()=>{
			puedeResponder = true
			pnlPreguntas.classList.add("cambio")
			alternativas.forEach(function(item){
				item.classList.remove("acierto", "error")
			})
		}, 2500)

		setTimeout(()=>{
			mostrarPregunta()
		}, 3000)
	}

	// maneja el botón salir para mostrar nuevamente el registro para un nuevo usuario
	const ftnSalir = e => {
		e.preventDefault()

		fetch("/listar", {method: "get"})
			.then(resultados => {
				return resultados.json()
			})
			.then(registros => {
				preguntas = registros

				resetear()

				pnlIntro.style.display = "block"
				pnlPreguntas.style.display = "none"
				pnlResultado.style.display = "none"
				pnlRuleta.style.display = "none"
			})
	}

	// maneja el botón seguir para que el usuario vuelva a jugar
	const ftnSeguir = e => {
		e.preventDefault()

		fetch("/listar", {method: "get"})
			.then(resultados => {
				return resultados.json()
			})
			.then(registros => {
				preguntas = registros

				resetear()

				pnlIntro.style.display = "none"
				pnlPreguntas.style.display = "block"
				pnlResultado.style.display = "none"
				pnlRuleta.style.display = "none"

				mostrarPregunta()
			})
	}

	// hace girar la ruleta
	const ftnGirar = ()=>{
		desacelerar = false
		aceleracion = Math.round(Math.random()*14+8)/1000
		intGiro = setInterval(()=>{
			if(!desacelerar) {
				if(velocidad<maxVelocidad){
					velocidad+=aceleracion
				} else {
					desacelerar = true
				}
			} else {
				if(velocidad>0) {
					velocidad-=aceleracion
				} else {
					velocidad = 0
					clearInterval(intGiro)
				}
			}
			angulo += velocidad

			if(angulo > anguloAnterior + 60) {
				snd.currentTime = 0
				snd.play()
				anguloAnterior = angulo
			}

			disco.style.webkitTransform = `rotate(${angulo}deg)`
			disco.style.mozTransform = `rotate(${angulo}deg)`
			disco.style.msTransform = `rotate(${angulo}deg)`
			disco.style.oTransform = `rotate(${angulo}deg)`
			disco.style.transform = `rotate(${angulo}deg)`

		},5)
	}

	// muestra la siguiente pregunta
	const mostrarPregunta = () => {
		preguntaActual++

		if(preguntaActual<=1) {
			if(numPreguntasAcertadas>0) {
				pnlIntro.style.display = "none"
				pnlPreguntas.style.display = "none"
				pnlResultado.style.display = "none"
				pnlRuleta.style.display = "block"			
			} else {
				enunciado.innerHTML = preguntas[preguntaActual].pregunta
				alt01.innerHTML = preguntas[preguntaActual].alternativa1
				alt02.innerHTML = preguntas[preguntaActual].alternativa2
				alt03.innerHTML = preguntas[preguntaActual].alternativa3
			}
		} else {
			pnlIntro.style.display = "none"
			pnlPreguntas.style.display = "none"

			if(numPreguntasAcertadas>0) {
				pnlResultado.style.display = "none"
				pnlRuleta.style.display = "block"			
			} else {
				pnlResultado.style.display = "block"
				pnlRuleta.style.display = "none"			
			}
		}
	}

	// maneja la apareción del teclado en el registro
	const ftnTeclado = function(e){

		if(e.target == destinoTexto) {
			if(cerrado) {
				cerrado = false
				teclado.classList.add("mostrar")
			} else {
				cerrado = true
				teclado.classList.remove("mostrar")
			}
		} else {
			cerrado = false
			teclado.classList.add("mostrar")
		}

		destinoTexto = e.target

		if(destinoTexto.innerHTML.toUpperCase()=="NOMBRE" || destinoTexto.innerHTML.toUpperCase()=="EMPRESA" || destinoTexto.innerHTML.toUpperCase()=="CORREO"){
			destinoTexto.innerHTML = ""
		}
	}

	// maneja la tecla presionada
	const ftnClicTecla = function(e){
		let valor=destinoTexto.innerHTML
		const tecla = this.innerHTML

		if(tecla=="bor") {
			valor = valor.substr(0,valor.length-1)
		} else if(tecla=="esp") {
			valor = `${valor} `
		} else {
			valor = `${valor}${tecla}`
		}

		destinoTexto.innerHTML = valor
	}

	// agrega listeners a cada alternativa
	alternativas.forEach(item => {
		item.addEventListener("click", ftnAlternativaSeleccionada)
	})

	// agrega listeners a cada input para abrir o cerra el teclado
	textos.forEach(item => {
		item.addEventListener("click", ftnTeclado)
	})

	// agrega listeners a cada tecla del teclado
	teclas.forEach(item => {
		item.addEventListener("click", ftnClicTecla)
	})

	// intercepta el fin de la animación de pase de pregunta
	pnlPreguntas.addEventListener("animationend", function(e){
		this.classList.remove("cambio")
	})

	// Botones
	btnIntro.addEventListener("click", ftnRegistro)
	btnSalir.addEventListener("click", ftnSalir)
	btnSeguir.addEventListener("click", ftnSeguir)
	btnGirar.addEventListener("click", ftnGirar)
	btnHome.addEventListener("click", ftnSalir)

	// Rest
	fetch("/listar", {method: "get"})
		.then(resultados => {
			return resultados.json()
		})
		.then(registros => {
			preguntas = registros
		})
}

// Todo se ejecuta cuando ha cargado el DOM
window.addEventListener("DOMContentLoaded", domCargado)